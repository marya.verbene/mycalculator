package calculator.model;

import calculator.exceptions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
@ConfigurationProperties("config")
public class Validation {

    @Autowired
    private WrongInputException wrongInputException;
    @Autowired
    private WrongBracketsException wrongBracketsException;
    @Autowired
    private TooLongException tooLongException;
    @Autowired
    private InvalidCharactersException invalidCharactersException;
    @Autowired
    private NullException nullException;

    private String inputStringForValidation;
    private String validatedString;
    private int inputLength;

    public void setWrongInputException(WrongInputException wrongInputException) {
        this.wrongInputException = wrongInputException;
    }

    public void setWrongBracketsException(WrongBracketsException wrongBracketsException) {
        this.wrongBracketsException = wrongBracketsException;
    }

    public void setTooLongException(TooLongException tooLongException) {
        this.tooLongException = tooLongException;
    }

    public void setInvalidCharactersException(InvalidCharactersException invalidCharactersException) {
        this.invalidCharactersException = invalidCharactersException;
    }

    public void setInputStringForValidation(String inputStringForValidation) {
        this.inputStringForValidation = inputStringForValidation;
    }

    public void setNullException(NullException nullException) {
        this.nullException = nullException;
    }

    public String getValidatedString() {
        return validatedString;
    }

    public void setInputLength(int inputLength) {
        this.inputLength = inputLength;
    }

    public void validate() throws Exception {

        inputStringForValidation = inputStringForValidation.replaceAll("\\s+", "");
        inputStringForValidation = inputStringForValidation.trim();
        inputStringForValidation = inputStringForValidation.replaceAll(",", ".");

        if (inputStringForValidation.isEmpty() || inputStringForValidation.matches("[ ]")) {
            throw nullException;
        }
        if (inputStringForValidation.trim().length() > inputLength) {
            throw tooLongException;
        }
        if (!inputStringForValidation.matches("[+^/*0-9.,()-]+")) {
            throw invalidCharactersException;
        }
        if (inputStringForValidation.matches("[+^/*.,()-]+")
                || Pattern.compile("[(][+*/^.,]").matcher(inputStringForValidation).find()
                || Pattern.compile("[.,+*/^-][)]").matcher(inputStringForValidation).find()
                || Pattern.compile("[)][.,0-9]").matcher(inputStringForValidation).find()
                || Pattern.compile("[.,0-9][(]]").matcher(inputStringForValidation).find()
                || Pattern.compile("[(][)]").matcher(inputStringForValidation).find()
                || Pattern.compile("[)][(]").matcher(inputStringForValidation).find()
                || Pattern.compile("[0-9][.,][0-9]*[.,]").matcher(inputStringForValidation).find()
                || !Pattern.compile("^[0-9(-]").matcher(inputStringForValidation).find()
                || !Pattern.compile("[0-9)]$").matcher(inputStringForValidation).find()
                || Pattern.compile("[+*^/.,-][+*^/.,-]").matcher(inputStringForValidation).find()) {
            throw wrongInputException;
        }
        if (inputStringForValidation.codePoints().filter(s -> s == '(').count() !=
                inputStringForValidation.codePoints().filter(s -> s == ')').count()) {
            throw wrongBracketsException;
        }
        validatedString = inputStringForValidation;
    }
}
