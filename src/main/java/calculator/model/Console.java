package calculator.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class Console {

    private String inputString;
    @Autowired
    private Calculate calculate;
    @Autowired
    private Validation validation;
    @Autowired
    private Parsing parsing;

    @SuppressWarnings("InfiniteLoopStatement")
    public void startCalculations() {
        while (true) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try {
                inputString = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                validation.setInputStringForValidation(inputString);
                validation.validate();
                parsing.setInputValidatedString(validation.getValidatedString());
                calculate.setExpressionValues(parsing.parse());
                System.out.println("Calculated result: " + BigDecimal.valueOf(calculate.calculate()).setScale(2, RoundingMode.HALF_UP));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
