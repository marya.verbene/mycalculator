package calculator.operations;

import calculator.exceptions.DeleteNullException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Divide implements Operation {

    private Double a;
    private Double b;
    public final static int PRIORITY = 2;
    @Autowired
    private DeleteNullException deleteNullException;

    public void setDeleteNullException(DeleteNullException deleteNullException) {
        this.deleteNullException = deleteNullException;
    }

    public void setA(Double a) {
        this.a = a;
    }

    public void setB(Double b) {
        this.b = b;
    }

    @Override
    public double countResult() throws DeleteNullException {
        if (a == 0) {
            throw deleteNullException;
        } else {
            return b / a;
        }
    }
}
