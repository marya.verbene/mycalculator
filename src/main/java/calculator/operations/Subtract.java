package calculator.operations;

import org.springframework.stereotype.Component;

@Component
public class Subtract implements Operation {

    private Double a;
    private Double b;
    public final static int PRIORITY = 1;

    public void setA(Double a) {
        this.a = a;
    }

    public void setB(Double b) {
        this.b = b;
    }

    @Override
    public double countResult() {
        return b - a;
    }
}
