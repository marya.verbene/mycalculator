package calculator.operations;

import calculator.exceptions.DeleteNullException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OperationsFactory {

    @Autowired
    private Sum sum;
    @Autowired
    private Subtract subtract;
    @Autowired
    private Multiply multiple;
    @Autowired
    private Pow pow;
    @Autowired
    private Divide divide;

    public void setSum(Sum sum) {
        this.sum = sum;
    }

    public void setSubtract(Subtract subtract) {
        this.subtract = subtract;
    }

    public void setMultiple(Multiply multiple) {
        this.multiple = multiple;
    }

    public void setPow(Pow pow) {
        this.pow = pow;
    }

    public void setDivide(Divide divide) {
        this.divide = divide;
    }

    public static boolean isOperator(String operator) {
        return operator.matches("[+-/*^]");
    }

    public double count(double a, double b, String operator) throws DeleteNullException {
        if (operator.equals("+")) {
            sum.setA(a);
            sum.setB(b);
            return sum.countResult();
        }
        if (operator.equals("-")) {
            subtract.setA(a);
            subtract.setB(b);
            return subtract.countResult();
        }
        if (operator.equals("*")) {
            multiple.setA(a);
            multiple.setB(b);
            return multiple.countResult();
        }
        if (operator.equals("/")) {
            divide.setA(a);
            divide.setB(b);
            return divide.countResult();
        }
        if (operator.equals("^")) {
            pow.setA(a);
            pow.setB(b);
            return pow.countResult();
        } else {
            return 0;
        }
    }

    public int getPriority(char operator) {
        if (operator == '+') {
            return Sum.PRIORITY;
        }
        if (operator == '-') {
            return Subtract.PRIORITY;
        }
        if (operator == '*') {
            return Multiply.PRIORITY;
        }
        if (operator == '/') {
            return Divide.PRIORITY;
        }
        if (operator == '^') {
            return Pow.PRIORITY;
        } else {
            return 0;
        }
    }
}
