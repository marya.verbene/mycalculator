package calculator.exceptions;

import org.springframework.stereotype.Component;

@Component
public class WrongInputException extends Exception {

    public WrongInputException() {
        super("WARNING: Expression is not valid. " +
                "Please check whether expression is correct (for instance, operators and digit values are situated properly) and enter it again.");
    }
}
