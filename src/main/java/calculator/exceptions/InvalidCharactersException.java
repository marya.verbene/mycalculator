package calculator.exceptions;

import org.springframework.stereotype.Component;

@Component
public class InvalidCharactersException extends Exception {

    public InvalidCharactersException() {
        super("WARNING: Entered expression contains invalid characters. Please enter another one.");
    }
}
