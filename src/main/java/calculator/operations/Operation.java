package calculator.operations;

import calculator.exceptions.DeleteNullException;

public interface Operation {
    double countResult () throws DeleteNullException;
}
