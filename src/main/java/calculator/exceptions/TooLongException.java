package calculator.exceptions;

import org.springframework.stereotype.Component;

@Component
public class TooLongException extends Exception {

    public TooLongException() {
        super("WARNING: Entered exception seems to be too long.");
    }
}
