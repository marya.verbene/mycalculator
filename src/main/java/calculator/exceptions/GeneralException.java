package calculator.exceptions;

import org.springframework.stereotype.Component;

@Component
public class GeneralException extends Exception {

    public GeneralException() {
        super("WARNING: Something went wrong during calculation - probably expression is not valid. " +
                "Please check it one more time.");
    }
}
