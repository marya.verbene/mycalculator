package calculator.model;

import calculator.exceptions.*;
import calculator.operations.*;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CalculateTest {

    private Calculate calculate;
    private Parsing parsing;
    private Validation validation;
    private GeneralException generalException;
    private DeleteNullException deleteNullException;
    private WrongBracketsException wrongBracketsException;
    private WrongInputException wrongInputException;
    private TooLongException tooLongException;
    private InvalidCharactersException invalidCharactersException;
    private  NullException nullException;

    @BeforeEach
    private void setup() {
        calculate = new Calculate();
        parsing = new Parsing();
        validation = new Validation();
        generalException = new GeneralException();
        deleteNullException = new DeleteNullException();
        wrongBracketsException = new WrongBracketsException();
        wrongInputException = new WrongInputException();
        tooLongException = new TooLongException();
        invalidCharactersException = new InvalidCharactersException();
        nullException = new NullException();
        OperationsFactory operationsFactory = new OperationsFactory();
        validation.setWrongBracketsException(wrongBracketsException);
        validation.setWrongInputException(wrongInputException);
        validation.setTooLongException(tooLongException);
        validation.setInvalidCharactersException(invalidCharactersException);
        validation.setNullException(nullException);
        Multiply multiply = new Multiply();
        Divide divide = new Divide();
        divide.setDeleteNullException(deleteNullException);
        Subtract subtract = new Subtract();
        Pow pow = new Pow();
        Sum sum = new Sum();
        operationsFactory.setMultiple(multiply);
        operationsFactory.setDivide(divide);
        operationsFactory.setPow(pow);
        operationsFactory.setSubtract(subtract);
        operationsFactory.setSum(sum);
        parsing.setOperationsFactory(operationsFactory);
        calculate.setOperationsFactory(operationsFactory);
        calculate.setGeneralException(generalException);
        validation.setInputLength(200);
    }

    public void testOutputValue(String expression, double expectedValue) throws Exception {
        validation.setInputStringForValidation(expression);
        validation.validate();
        parsing.setInputValidatedString(validation.getValidatedString());
        calculate.setExpressionValues(parsing.parse());
        double result = Math.floor(calculate.calculate() * 100) / 100;
        Assert.assertEquals(expectedValue, result, 0.00);
    }

    public void testException(String expression, Exception expectedException) {
        Exception exception = new Exception();
        if (expectedException.equals(deleteNullException) || expectedException.equals(generalException)) {
            parsing.setInputValidatedString(expression);
            calculate.setExpressionValues(parsing.parse());
            try {
                calculate.calculate();
            } catch (Exception e) {
                exception = e;
            }
        } else {
            validation.setInputStringForValidation(expression);
            try {
                validation.validate();
            } catch (Exception e) {
                exception = e;
            }
        }
        Assert.assertSame(expectedException, exception);
    }

    @Test
    public void testOutputCase1() throws Exception {
        testOutputValue("(-2) - ((-4) * 3.5) ", 12);
    }

    @Test
    public void testOutputCase2() throws Exception {
        testOutputValue("(-6)-(((((-5+(-9))))))", 8);
    }

    @Test
    public void testOutputCase3() throws Exception {
        testOutputValue("-2*9.77-(9.333+28,78/9)*7,89", -118.41);
    }

    @Test
    public void testOutputCase4() throws Exception {
        testOutputValue("89/12*78+(1-9)", 570.5);
    }

    @Test
    public void testOutputCase5() throws Exception {
        testOutputValue("(-5)+9*8-16+(-5)-5", 41);
    }

    @Test
    public void testExceptionCase1() {
        testException("(25/7)/(0/5)", deleteNullException);
    }

    @Test
    public void testExceptionCase2() {
        testException("(-5)+9**8-16+(-5)-5", wrongInputException);
    }

    @Test
    public void testExceptionCase3() {
        testException("-2*9.77-((9.333+28,78/9)*7,89", wrongBracketsException);
    }

    @Test
    public void testExceptionCase4() {
        testException("(5+9)%8)", invalidCharactersException);
    }

    @Test
    public void testExceptionCase5() {
        testException("89/-12*78+(1-9)", wrongInputException);
    }

    @Test
    public void testExceptionCase6() {
        testException(Stream.generate(() -> "1").limit(201).collect(Collectors.joining()), tooLongException);
    }

    @Test
    public void testExceptionCase7() {
        testException("((569)+8-)", wrongInputException);
    }

    @Test
    public void testExceptionCase8() {
        testException(" ", nullException);
    }
}