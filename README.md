Simple calculator application which maintains the following operations:
summing (+), 
subtraction (-), 
division (/), 
multiplication (*), 
exponentiation (^).

Exceptions handled:
invalid input (characters except digits or bracets or mathematical operators listed above) or empty value,
starting of expression with non-digit character (except '-' - it is allowed), 
wrong number of brackets, 
total length of expression is limited now by 200 characters (may be adjusted via application.properties), 
wrong input of operators and arguments in general, 
division on zero and wrong formatting. 
Division by zero is handled during calculation process while other exceptions are checked during validation stage.
Also during calculationg stage some general exception is handled in case of any unregistered exception turns up. 


Some application features:
integer/float format of digits (with separator '.' or ',' does not matter), 
negative values (they can be included in parenthesis or not but please note that repeated operators are not allowed), 
bracets, 
spaces.

Mathematical expression for calculation have to be entered in command line without "=" so like:
(-2) - ((-4) * 3.5) 
and after pressing 'Enter' the result should be displayed as follows: 
Calculated result: 12.0 
or infoming message can be displayed in case of failure. 
Then you may proceed with calculations further. 
