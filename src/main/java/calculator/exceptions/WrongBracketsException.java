package calculator.exceptions;

import org.springframework.stereotype.Component;

@Component
public class WrongBracketsException extends Exception {

    public WrongBracketsException() {
        super("WARNING: Please check the number of opening and closing brackets in the expression and enter it again.");
    }
}
