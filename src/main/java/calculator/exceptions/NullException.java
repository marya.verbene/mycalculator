package calculator.exceptions;

import org.springframework.stereotype.Component;

@Component
public class NullException extends Exception {

    public NullException() {
        super("WARNING: No expression was entered.");
    }
}
