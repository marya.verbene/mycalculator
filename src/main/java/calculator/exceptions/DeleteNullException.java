package calculator.exceptions;

import org.springframework.stereotype.Component;

@Component
public class DeleteNullException extends Exception {

    public DeleteNullException() {
        super("WARNING: Expression contains division by 0. Please enter another one.");
    }
}
