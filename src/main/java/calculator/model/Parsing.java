package calculator.model;

import calculator.operations.OperationsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayDeque;

@Component
public class Parsing {

    private String inputValidatedString;
    @Autowired
    private OperationsFactory operationsFactory;

    public void setOperationsFactory(OperationsFactory operationsFactory) {
        this.operationsFactory = operationsFactory;
    }

    public void setInputValidatedString(String inputValidatedString) {
        this.inputValidatedString = inputValidatedString;
    }

    public ArrayDeque<String> parse() {
        StringBuilder digit = new StringBuilder();
        ArrayDeque<Character> operators = new ArrayDeque<>();
        ArrayDeque<String> parsedExpression = new ArrayDeque<>();
        char previousElement = ' ';

        for (int i = 0; i < inputValidatedString.length(); i++) {
            char element = inputValidatedString.charAt(i);

            if (Character.isDigit(element)) {
                digit.append(element);
            } else if (element == '.') {
                digit.append(element);
            } else {
                if (!digit.toString().equals("")) {
                    parsedExpression.push(digit.toString());
                    digit = new StringBuilder();
                }
                if (OperationsFactory.isOperator(String.valueOf(element))) {
                    if (!operators.isEmpty()) {
                        if (element == '-' && previousElement == '(') {
                            digit.append(element);
                        } else if (operationsFactory.getPriority(element) <= operationsFactory.getPriority(operators.peek())) {
                            parsedExpression.push(String.valueOf(operators.pop()));
                            operators.push(element);
                        } else {
                            operators.push(element);
                        }
                    } else {
                        if (element == '-' && i == 0) {
                            digit.append(element);
                        } else {
                            operators.push(element);
                        }
                    }
                } else if (element == '(') {
                    operators.push(element);
                } else if (element == ')') {
                    while (operators.peek() != '(') {
                        parsedExpression.push(String.valueOf(operators.pop()));
                    }
                    operators.pop();
                }
            }
            previousElement = element;
        }
        if (!digit.toString().equals("")) {
            parsedExpression.push(digit.toString());
        }
        if (!operators.isEmpty()) {
            while (!operators.isEmpty()) {
                parsedExpression.push(String.valueOf(operators.pop()));
            }
        }
        return parsedExpression;
    }
}
