package calculator.model;

import calculator.exceptions.DeleteNullException;
import calculator.exceptions.GeneralException;
import calculator.operations.OperationsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayDeque;
import java.util.Iterator;

@Component
public class Calculate {

    private ArrayDeque<String> expressionValues;
    @Autowired
    private OperationsFactory operationsFactory;
    @Autowired
    private GeneralException generalException;

    public void setOperationsFactory(OperationsFactory operationsFactory) {
        this.operationsFactory = operationsFactory;
    }

    public void setGeneralException(GeneralException generalException) {
        this.generalException = generalException;
    }

    public void setExpressionValues(ArrayDeque<String> expressionValues) {
        this.expressionValues = expressionValues;
    }

    public double calculate() throws DeleteNullException, GeneralException {
        ArrayDeque<Double> resultStack = new ArrayDeque<>();
        Iterator<String> it = expressionValues.descendingIterator();
        double a;
        double b;
        double operationResult;
        String element;

        while (it.hasNext()) {
            element = it.next();
            if (!OperationsFactory.isOperator(element)) {
                resultStack.push(Double.parseDouble(element));
            } else {
                try {
                    a = resultStack.pop();
                    b = resultStack.pop();
                } catch (Exception e) {
                    throw generalException;
                }
                operationResult = operationsFactory.count(a, b, element);
                resultStack.push(operationResult);
            }
        }
        return resultStack.pop();
    }
}
